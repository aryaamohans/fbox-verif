import random
import sys
import cocotb
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from testfloat_model import *
import logging as log

fh = open("log_compare_dp.txt","w")
class InputDriver(BusDriver):
    """ Drives inputs of DUT. """
    _signals = ["sendreceive_x","sendreceive_y", "EN_sendreceive"]
    
    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)

class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, sendreceive_x=0, sendreceive_y=0,EN_sendreceive=0):
        self.sendreceive_x = BinaryValue(sendreceive_x, tb.sendreceive_x_bits, False)
        self.sendreceive_y = BinaryValue(sendreceive_y, tb.sendreceive_y_bits, False)
        self.EN_sendreceive = BinaryValue(EN_sendreceive, tb.EN_sendreceive_bit, False)


class InputMonitor(BusMonitor):
    """Observes inputs of DUT."""
    _signals = ["sendreceive_x","sendreceive_y", "EN_sendreceive"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
            clkedge = RisingEdge(self.clock)

            while True:
                yield clkedge
                vec = (self.bus.sendreceive_x.value.integer,self.bus.sendreceive_x.value.integer,self.bus.EN_sendreceive.value.integer)
                self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / received by OutputMonitor."""
    def __init__(self, tb=None, sendreceive=0, RDY_sendreceive=0):
        """For expected transactions, value 'None' means don't care. tb must be an instance of the Testbench class."""
        if sendreceive is not None and isinstance(sendreceive, int):
            sendreceive = BinaryValue(sendreceive, tb.sendreceive_bits, False)
        if RDY_sendreceive  is not None and isinstance(RDY_sendreceive, int):     
            RDY_sendreceive = BinaryValue(RDY_sendreceive, tb.RDY_sendreceive_bits, False)
        self.value = [sendreceive]

class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = [ "sendreceive", "RDY_sendreceive","EN_sendreceive"]
    
    def __init__(self, dut, tb, callback=None, event=None):
        """tb must be an instance of the Testbench class."""
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb
   
    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
   
        while True:
            yield clkedge
            if (self.bus.RDY_sendreceive.value==1) and (self.bus.EN_sendreceive.value ==1):
                vec =(self.bus.sendreceive.value)
                self._recv(OutputTransaction(self.tb,vec))

                                                     

class Testbench(object):
    print_task='Testbench'
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got1=got.value[0]
            exp1=exp.value[0]
            #fh.write("\nExpected: {0!s}.\nReceived: {1!s}.".format(exp1, got1))
            LT=0
            EQ=1
            GT=2
            UO=3
            if(exp1 == 1):
               if(got1==LT) or (got==EQ):
                   fh.write(' passed\n')
            if(exp1 ==0):
               if (got1!=LT) and (got1!=EQ):
                   fh.write(' passed\n')
               else:
                   fh.write(' Expected. differ Received\n')
                   exit(1)
 
    
    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.sendreceive_x_bits = 64
        self.sendreceive_y_bits = 64
        self.EN_sendreceive_bit =1
        self.sendreceive_bits =3 
        self.RDY_sendreceive_bits  =1
        init_val = OutputTransaction(self)
        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)
        
        # scoreboard on the outputs
        self.expected_output = []
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)
        
        # Reconstruct the input transactions from the pins
        # and send them to our 'model'
        self.input_mon = InputMonitor(dut)
    

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True

def random_input_gen(tb,n=1):
    """
    Generate random input data to be applied by InputDriver.
    Returns up to n instances of InputTransaction.
    tb must an instance of the Testbench class.
    """
    fh.write('INPUT_GEN')
    func_list = ["f64_le"]
    round_mode=["near_even","near_maxMag","minMag","max","min"]
    for rmode in round_mode:
        inp1,inp2, exp = run_testfloat('f64_le', rmode, 0, 2)
        if len(inp1) == 0 or len(inp2) ==0 or len(exp) == 0:
            log.error("\ntestfloat did not generate test. Try increasing timeout_value")
        fh.write(" \nTest vectors and expected outputs from model")
        fh.write("\n============================================================================")
         
        fh.write("\nle(dp)  :total expected testcases {0!s} total inp1 {1!s} inp2 {2!s} , round_mode {3}".format(len(exp),len(inp1),len(inp2),rmode))
        for e in range(len(exp)):
            
            #fh.write(" --------expout---{0}".format(exp[e]))
            tb.expected_output.append( OutputTransaction(tb, exp[e]) )
        
        for i ,j in zip(range(len(inp1)),range(len(inp2))):
            print_task = 'random_input_gen'
            fh.write("\n{0:20s} | === inp transaction === {1}, {2} rmode {3}".format(print_task, hex(inp1[i]),hex(inp2[j]),rmode))
            #if(i < len(inp1)):
            yield InputTransaction(tb, inp1[i],inp2[j], 1)
 

@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(5) # ps
        signal <= 1
        yield Timer(5) # ps
    
@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 0
    yield Timer(10, units='ps')
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    
    # Issue first transaction immediately.
    yield tb.input_drv.send(input_gen, False)
    
    # Issue next transactions.
    for t in input_gen:
        yield tb.input_drv.send(t)
    
    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)
    
