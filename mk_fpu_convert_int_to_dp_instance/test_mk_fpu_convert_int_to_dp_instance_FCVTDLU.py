import random
import sys
import logging as log
import cocotb
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from testfloat_model import *
import logging as log

fh = open("log_FCVTDLU.txt","w")
class InputDriver(BusDriver):
    """ Drives inputs of DUT. """
    _signals = ["start_inp_int","start_unsigned_bit","start_rounding_mode","start_long", "EN_start"]
    
    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)

class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, start_inp_int=0,start_unsigned_bit=0,start_rounding_mode=0,start_long=0, EN_start=0):
        self.start_inp_int = BinaryValue(start_inp_int, tb.start_inp_int_bits, False)
        self.start_unsigned_bit = BinaryValue(start_unsigned_bit, tb.start_unsigned_bit_bits, False)
        self.start_long = BinaryValue(start_long, tb.start_long_bits, False)
        self.start_rounding_mode = BinaryValue(start_rounding_mode, tb.start_rounding_mode_bits, False)
        self.EN_start = BinaryValue(EN_start, tb.EN_start_bit, False)


class InputMonitor(BusMonitor):
    """Observes inputs of DUT."""
    _signals = ["start_inp_int","start_unsigned_bit","start_rounding_mode","start_long", "EN_start"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
            clkedge = RisingEdge(self.clock)

            while True:
                yield clkedge
                vec = (self.bus.start_inp_int.value.integer,self.bus.start_unsigned_bit.value.integer,self.bus.start_long.value.integer,self.bus.start_rounding_mode.value.integer,self.bus.EN_start.value.integer)
                self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / received by OutputMonitor."""
    def __init__(self, tb=None, receive=0, RDY_receive=0,RDY_start=0):
        """For expected transactions, value 'None' means don't care. tb must be an instance of the Testbench class."""
        if receive is not None and isinstance(receive, int):
            receive = BinaryValue(receive, tb.receive_bits, False)
        if RDY_receive  is not None and isinstance(RDY_receive, int):     
            RDY_receive = BinaryValue(RDY_receive, tb.RDY_receive_bits, False)
        if RDY_start  is not None and isinstance(RDY_start, int):     
            RDY_start = BinaryValue(RDY_start, tb.RDY_start_bits, False)
        self.value = [receive]

class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = [ "receive", "RDY_receive","RDY_start","EN_start","start_inp_int"]
    
    def __init__(self, dut, tb, callback=None, event=None):
        """tb must be an instance of the Testbench class."""
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb
   
    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
   
        while True:
            yield clkedge
            if (self.bus.RDY_receive.value==1) and (self.bus.RDY_start.value ==1) and (self.bus.EN_start.value ==1):
                valid =(self.bus.receive.value)>>69
                if valid==1:
                    vec =(self.bus.receive.value)
                    fh.write("\ninput transaction:    {0} ".format(hex(self.bus.start_inp_int.value)))
                    self._recv(OutputTransaction(self.tb,vec))

                                                     

class Testbench(object):
    print_task='Testbench'
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got1=got.value[0]
            exp1=exp.value[0]
            fh.write("\nExpected: {0!s}.\nReceived: {1!s}.".format(hex(exp1), hex(got1)))
            flag_got=got1 & 0x1F
            flag_exp=exp1 & 0x1F
            valid=got1>>69
            got1=got1>>5
            exp1=exp1>>5
            if valid ==1:
                if got1 != exp1:
                    fh.write("OUTPUT Expected: {0!s}.\ndiffer Received: {1!s}.\n".format(hex(exp1), hex(got1)))
                    exit(1)
                elif flag_got != flag_exp:
                    fh.write("FLAGS  Expected: {0!s}.\ndiffer Received: {1!s}.\n".format(bin(flag_exp), bin(flag_got)))
                    exit(1)
                   
                else:
                    fh.write(' passed\n') 

    
    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.start_inp_int_bits = 64
        self.EN_start_bit =1
        self.RDY_start_bits =1
        self.receive_bits =70 
        self.RDY_receive_bits  =1
        self.start_unsigned_bit_bits  =1
        self.start_long_bits  =1
        self.start_rounding_mode_bits  =3
        init_val = OutputTransaction(self)
        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)
        
        # scoreboard on the outputs
        self.expected_output = []
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)
        
        # Reconstruct the input transactions from the pins
        # and send them to our 'model'
        self.input_mon = InputMonitor(dut)
    

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True

def random_input_gen(tb,n=1):
    """
    Generate random input data to be applied by InputDriver.
    Returns up to n instances of InputTransaction.
    tb must an instance of the Testbench class.
    """
    fh.write('\nINPUT_GEN')
    func_list = ["ui64_to_f64"]
    round_mode=["near_even","near_maxMag","minMag","max","min"]
    for rmode in round_mode:
        inp, exp = run_testfloat('ui64_to_f64', rmode, 0, 5)
        if len(inp) == 0 or len(exp) == 0:
            log.error("\ntestfloat did not generate test. Try increasing timeout_value")
        fh.write(" \nTest vectors and expected outputs from model")

        fh.write("\n============================================================================")
         
        fh.write("\nint to dp(FCVT.D.LU)  :total expected testcases {0!s} total inp {1!s}  , round_mode {2}".format(len(exp),len(inp),rmode))

        
        for e in range(len(exp)):
            #fh.write" -----------",bin(exp[0]))
            tb.expected_output.append( OutputTransaction(tb, exp[e]) )
        
        for i in range(len(inp)):
            if(rmode == 'near_even'):
                   yield InputTransaction(tb, inp[i],1,0,1,1)
            if(rmode == 'near_maxMag'):
                   yield InputTransaction(tb, inp[i],1,4,1,1)
            if(rmode == 'minMag'):
                   yield InputTransaction(tb, inp[i],1,1,1,1)
            if(rmode == 'min'):
                   yield InputTransaction(tb, inp[i],1,2,1,1)
            if(rmode == 'max'):
                   yield InputTransaction(tb, inp[i],1,3,1,1)

 

@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(5) # ps
        signal <= 1
        yield Timer(5) # ps
    
@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 0
    yield Timer(10, units='ps')
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    
    # Issue first transaction immediately.
    #yield tb.input_drv.send(input_gen, False)
    
    # Issue next transactions.
    for t in input_gen:
        yield tb.input_drv.send(t)
    
    #yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)
    
