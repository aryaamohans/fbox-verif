import subprocess
import shlex
from enum import Enum
from datetime import datetime
from random import randint
# exception bit position in an 8 bit hex in testfloat
# reverse the 5 bits for design

#bit 0   	inexact exception
#bit 1	underflow exception
#bit 2	overflow exception
#bit 3	infinite exception (“divide by zero”)
#bit 4	invalid exception
class Exception(Enum):
    inexact_exception = 0
    underflow_exception = 1
    overflow_exception = 2
    infinite_exception_div0 = 3
    invalid_exception = 4

class InputSeq:
    def __init__(self, operand1, operand2, operand3, round_mode):
        self.operand1 = operand1
        self.operand2 = operand2
        self.operand3 = operand3
        self.round_mode = round_mode
    def get_item(self):
        return '{0} {1} {2} {3}'.format(self.operand1, self.operand2, self.operand3, self.round_mode)

class OutputSeq:
    def __init__(self, result, flag):
        self.result = result
        self.flag = flag
    def get_item(self):
        return '{0} {1}'.format(self.result, self.flag)


module_to_function_map = {
    'mk_fpu_add_sub_sp_instance' : 'f32_add',
    'mk_fpu_add_sub_dp_instance' : 'f64_add',
    'mk_fpu_fn_fp_compare_sp_instance' : '',
    'mk_fpu_fn_fp_compare_dp_instance' : '',
    'mk_fpu_fclass_sp_instance' : '',
    'mk_fpu_fclass_dp_instance' : '',
    'mk_fpu_convert_sp_to_dp_instance' : '',
    'mk_fpu_convert_dp_to_sp_instance' : '',
    'mk_fpu_convert_sp_to_int_instance' : '',
    'mk_fpu_convert_dp_to_int_instance' : '',
    'mk_fpu_int_to_sp' : '',
    'mk_fpu_int_to_dp' : '',
    'mk_fpu_convert_sp_to_int_instance' : '',
    'mk_fma_sp_instance' : '',
    'mk_fma_dp_instance' : '',
        }
def sys_command(command):
    x = subprocess.Popen(shlex.split(command),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         )
    try:
        out, err = x.communicate(timeout=5)
    except TimeoutExpired:
        x.kill()
        out, err = x.communicate()

    out = out.rstrip()
    err = err.rstrip()
    return out.decode("ascii")

def run_testfloat(function, round_mode, seed, timeout_value, test_file=''):

    pk = sys_command('which pk')
    testfloat_gen = sys_command('which testfloat_gen')
    if (seed == 0):
        seed = randint(1, 65000)
    #command = 'spike {0} {1} -r{2} -seed {3} -level 2 {4}'.format(pk, testfloat_gen, 
    #                                                 round_mode, seed, function)
    if test_file:
        fp = open(test_file, 'r')
        gen = fp.read()
        fp.close()
    else:
#        command = 'qemu-riscv64 {0} {1}'.format( testfloat_gen, function)
        command = 'qemu-riscv64 {0} -r{1} -seed {2} -level 2 {3}'.format( testfloat_gen, 
                                                         round_mode, seed, function)
        print(command)
        proc = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        gen = ''
        try:
            out, err = proc.communicate(timeout=timeout_value)
        except subprocess.TimeoutExpired:
            proc.kill()
            out, err = proc.communicate()
            gen = out.decode('ascii')
            stampnow = datetime.now()
            filename = 'tf_{0}_{1}_{2}.test'.format(function, seed, stampnow.strftime("%Y%m%d%H%M%S%f"))
            fp = open(filename, 'w')
            fp.write(gen)
            fp.close()
    
    tests = gen.splitlines()
    #print(tests)

    input_list = []
    expected_list = []
    for test in tests:
        inp = ''
        if test != 'bbl loader':
            tlist = test.split(' ')
            #print(tlist)
            if function == 'f32_lt':
                if (tlist[3] == '00') and (tlist[2] == '1'):
                    inp = int((tlist[0]+tlist[1]), 16)
                    input_list.append(inp)
                    exp = int(bin(int((tlist[0]), 16))[2:] + '00000', 2)
                    exp =(exp & 0xFFFFFFFFFF) | 0x2000000000
                    expected_list.append(exp)


                   
    return input_list, expected_list

if __name__ == "__main__":
    run_testfloat('f32_add', 'near_even', 1,1)
