# FP Verification Setup

```bash

# verilog source path:
$ /scratch/gitlab-builds/releases/fbox/mk_fpu_add_sub_sp_instance/
$ export PATH="/home/gitlab-runner/.pyenv/bin:$PATH"
$ eval "$(pyenv init -)"
$ eval "$(pyenv virtualenv-init -)"
$ pyenv activate py36
$ source /tools/hw_tools/inc_tools/cocotb-verilator.sh
$ source /tools/sw_tools/qemu.sh
$ export PATH=/scratch/gitlab-builds/releases/common-verif/testfloat:$PATH
 

# simulation setup
$ cd fbox-verif
$ make SIM=verilator

or


# Simulation setup for River_block
$ cd fbox-verif
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_add_sub_dp_instance/mk_fpu_add_sub_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_add_sub_sp_instance/mk_fpu_add_sub_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_compare_dp_instance/mk_fpu_compare_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_compare_sp_instance/mk_fpu_compare_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_multiplier_dp_instance/mk_fpu_multiplier_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_multiplier_sp_instance/mk_fpu_multiplier_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_dp_to_int_instance/mk_fpu_convert_dp_to_int_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_sp_to_int_instance/mk_fpu_convert_sp_to_int_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_dp_to_sp_instance/mk_fpu_convert_dp_to_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_sp_to_dp_instance/mk_fpu_convert_sp_to_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_int_to_dp_instance/mk_fpu_convert_int_to_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_convert_int_to_sp_instance/mk_fpu_convert_int_to_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_fma_dp_instance/mk_fpu_fma_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_fma_sp_instance/mk_fpu_fma_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_max_dp_instance/mk_fpu_max_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_max_sp_instance/mk_fpu_max_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_min_dp_instance/mk_fpu_min_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_fpu_min_sp_instance/mk_fpu_min_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_divider_dp_instance/mk_divider_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_divider_sp_instance/mk_divider_sp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_sqrt_dp_instance/mk_sqrt_dp_instance.yaml
$ perl -pi -e 's/<repo_path>/$ENV{PWD}/g' mk_sqrt_sp_instance/mk_sqrt_sp_instance.yaml

# verification
$ cd fbox-veriif

      - cd mk_fpu_add_sub_dp_instance
      - river_block --env mk_fpu_add_sub_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_add_sub_sp_instance
      - river_block --env mk_fpu_add_sub_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_compare_dp_instance
      - river_block --env mk_fpu_compare_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_compare_sp_instance
      - river_block --env mk_fpu_compare_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_multiplier_dp_instance
      - river_block --env mk_fpu_multiplier_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_multiplier_sp_instance
      - river_block --env mk_fpu_multiplier_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_dp_to_int_instance
      - river_block --env mk_fpu_convert_dp_to_int_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_sp_to_int_instance
      - river_block --env mk_fpu_convert_sp_to_int_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_dp_to_sp_instance
      - river_block --env mk_fpu_convert_dp_to_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_sp_to_dp_instance
      - river_block --env mk_fpu_convert_sp_to_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_int_to_dp_instance
      - river_block --env mk_fpu_convert_int_to_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_convert_int_to_sp_instance
      - river_block --env mk_fpu_convert_int_to_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_fma_dp_instance
      - river_block --env mk_fpu_fma_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_fma_sp_instance
      - river_block --env mk_fpu_fma_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_max_dp_instance
      - river_block --env mk_fpu_max_dp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_max_sp_instance
      - cd ../mk_fpu_min_dp_instance
      - river_block --env mk_fpu_max_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_min_sp_instance
      - river_block --env mk_fpu_min_sp_instance.yaml -clst --verbose debug
      - cd ../mk_divider_dp_instance
      - river_block --env mk_divider_dp_instance.yaml -clst --verbose debug
      - cd ../mk_divider_sp_instance
      - river_block --env mk_divider_sp_instance.yaml -clst --verbose debug
      - cd ../mk_fpu_multiplier_dp_instance
      - cd ../mk_sqrt_dp_instance
      - river_block --env mk_sqrt_dp_instance.yaml -clst --verbose debug
      - cd ../mk_sqrt_sp_instance
      - river_block --env mk_sqrt_sp_instance.yaml -clst --verbose debug
      

## Verification with testfloat and cocotb Status

+-------------------+--------------+------------+------------+------------+-------------+----------+
| Function/Rounding | near_even    | towards_0  | round_down | round_up   | near_maxMag | Comments |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fadd (sp)         | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (6M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fadd (dp)         | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fsub (sp)         | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fsub (dp)         | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmul (sp)         | Pass (5M)    | Pass (5M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmul (dp)         | Pass (5M)    | Pass (5M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fdiv (sp)         | Pass (1M)    | Pass (1M)  | Pass (1M)  | Pass (1M)  | Pass (1M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fdiv (dp)         | Pass (1M)    | Pass (1M)  | Pass (1M)  | Pass (1M)  | Pass (1M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fsqrt(sp)         | Pass (1M)    | Pass (1M)  | Pass (1M)  | Pass (1M)  | Pass (1M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fsqrt (dp)        | Pass (1M)    | Pass (1M)  | Pass (1M)  | Pass (1M)  | Pass (1M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fle (sp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fle (dp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| flt (sp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| flt (dp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| feq (sp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| feq (dp)          | Pass (7M)    | Pass (7M)  | Pass (7M)  | Pass (7M)  | Pass (7M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmin (sp)         | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmin (dp)         | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmax (sp)         | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmax (dp)         | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.D.W          | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.D.WU         | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.D.L          | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.D.LU         | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.S.W          | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.S.WU         | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.S.L          | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.S.LU         | Pass (5M)    | Pass (5M)  | Pass (5M)  | Pass (5M)  | Pass (5M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fsp_to_dp         | Pass (10K)   | Pass (10K) | Pass (10K) | Pass (10K) | Pass (10K)  |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fdp_to_sp         | Pass (10K)   | Pass (26K) | Pass (26K) | Pass (26K) | Pass (26K)  |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmadd (sp)        | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| fmadd (dp)        | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.WU.S         | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.W.S          | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.L.S          | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.L.U.S        | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.L.D          | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.L.U.D        | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
|  FCVT.WU.S        | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |          |
+-------------------+--------------+------------+------------+------------+-------------+----------+
| FCVT.W.D          | Pass (4M)    | Pass (4M)  | Pass (4M)  | Pass (4M)  | Pass (4M)   |  Flags   |
+-------------------+--------------+------------+------------+------------+-------------+----------+
