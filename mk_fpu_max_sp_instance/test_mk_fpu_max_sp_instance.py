import random
import sys
import cocotb
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from testfloat_model import *
import logging as log

fh = open("log_max_sp.txt","w")
class InputDriver(BusDriver):
    """ Drives inputs of DUT. """
    _signals = ["start_inp", "EN_start"]
    
    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)

class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, start_inp=0, EN_start=0):
        self.start_inp = BinaryValue(start_inp, tb.start_inp_bits, False)
        self.EN_start = BinaryValue(EN_start, tb.EN_start_bit, False)


class InputMonitor(BusMonitor):
    """Observes inputs of DUT."""
    _signals = ["start_inp", "EN_start"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
            clkedge = RisingEdge(self.clock)

            while True:
                yield clkedge
                vec = (self.bus.start_inp.value.integer,self.bus.EN_start.value.integer)
                self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / startd by OutputMonitor."""
    def __init__(self, tb=None, start=0, RDY_start=0):
        """For expected transactions, value 'None' means don't care. tb must be an instance of the Testbench class."""
        if start is not None and isinstance(start, int):
            start = BinaryValue(start, tb.start_bits, False)
        if RDY_start  is not None and isinstance(RDY_start, int):     
            RDY_start = BinaryValue(RDY_start, tb.RDY_start_bits, False)
        self.value = [start]

class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = [ "start", "RDY_start","RDY_start","EN_start"]
    
    def __init__(self, dut, tb, callback=None, event=None):
        """tb must be an instance of the Testbench class."""
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb
   
    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
   
        while True:
            yield clkedge
            if (self.bus.RDY_start.value ==1)  and (self.bus.EN_start.value ==1):
                vec =(self.bus.start.value)
                self._recv(OutputTransaction(self.tb,vec))

                                                     

class Testbench(object):
    print_task='Testbench'
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got1=got.value[0]
            exp1=exp.value[0]
            #fh.write("\nExpected: {0!s}.\nReceived: {1!s}.".format(exp1, got1))
            if got1 != exp1:
                #fh.write(" Expected: {0!s}. differ Received: {1!s}.\n".format(hex(int(exp1,2)), hex(int(got1,2))))
                #fh.write("       Expected: {0!s}.\ndiffer Received: {1!s}.".format(exp1,got1))
                fh.write("       Expected: {0!s}. differ Received: {1!s}.\n".format(hex(exp1), hex(got1)))
                exit(1)
            else:
                fh.write(' passed\n')

    
    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.start_inp_bits = 64
        self.EN_start_bit =1
        self.start_bits = 38
        self.RDY_start_bits  =1
        self.RDY_start_bits  =1
        init_val = OutputTransaction(self)
        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)
        
        # scoreboard on the outputs
        self.expected_output = []
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)
        
        # Reconstruct the input transactions from the pins
        # and send them to our 'model'
        self.input_mon = InputMonitor(dut)
    

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True

def random_input_gen(tb,n=1):
    """
    Generate random input data to be applied by InputDriver.
    Returns up to n instances of InputTransaction.
    tb must an instance of the Testbench class.
    """
    fh.write('\nINPUT_GEN')
    #inp, exp = run_testfloat('f64_le, 'near_even', 0, 1, '/home/divya/fbox_tb/fbox-verif/tf_f64_add_1_20200318103931817244.test')
    func_list = ["f32_lt"]
    roundmode_list = ["near_even","near_maxMag","minMag","min","max"]
    for rmode in roundmode_list:
        inp, exp = run_testfloat('f32_lt', rmode, 1, 1)
        if len(inp) == 0 or len(exp) == 0:
            log.error("\ntestfloat did not generate test. Try increasing timeout_value")
        fh.write(" \nTest vectors and expected outputs from model")
        fh.write("\n============================================================================")
         
        fh.write("\nmax(sp)  :total expected testcases {0!s} total inp {1!s}  , round_mode {2}".format(len(exp),len(inp),rmode))

        
        
        for e in range(len(exp)):
            #fh.write(" -----------",bin(exp[e]))
            tb.expected_output.append( OutputTransaction(tb, exp[e]) )
        for i in range(len(inp)):
            print_task = 'random_input_gen'
            fh.write("\n{0:20s} | === inp transaction === {1} round_mode {2}".format(print_task, hex(int(inp[i])),rmode))
            yield InputTransaction(tb, inp[i], 1)
 

@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(5) # ps
        signal <= 1
        yield Timer(5) # ps
    
@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 0
    yield Timer(10, units='ps')
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    
    # Issue first transaction immediately.
    yield tb.input_drv.send(input_gen, False)
    
    # Issue next transactions.
    for t in input_gen:
        yield tb.input_drv.send(t)
    
    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)
    
