import cocotb
from cocotb.result import TestFailure, TestSuccess
from cocotb.triggers import Timer, RisingEdge
from cocotb.clock import Clock
import subprocess
import shlex
from cocotb.binary import BinaryValue
from cocotb.binary import BinaryRepresentation
from random import randint
import random
import sys
import logging as log
import math 
from cocotb.decorators import coroutine
import random

fh = open("log_sqrt_sp.txt","w")
N = 10000 # No. of testcases 
validBit = BinaryValue(int("10000000000000000000000000000000000000",2),n_bits=70,bigEndian=False)

#=========== FPU AddSub design Interface ==========================
#  Ports:
#  Name                         I/O  size props
#  CLK                            I     1 clock
#  RST_N                          I     1 reset
#  send_operands                  I    67
#  EN_send                        I     1
#  RDY_send                       O     1 const
#  receive                        O    38 reg  
#  RDY_receive                    O     1 const
#==================================================================


def sys_command(command):
    x = subprocess.Popen(shlex.split(command),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         )
    try:
        out, err = x.communicate(timeout=100)
    except TimeoutError:
        x.kill()
        out, err = x.communicate()

    out = out.rstrip()
    err = err.rstrip()
    return out.decode("ascii")

def run_testfloat(function, round_mode, level, file_name):
    testfloat_gen_path = sys_command("which testfloat_gen")
    seed = randint(1,2)
    command = "qemu-riscv64 {0} -r{1} -seed {2} -level {3} {4}".format(testfloat_gen_path,round_mode,seed,level,function)    
    
    testgen = sys_command(command)
    fp = open(file_name,"w")
    fp.write(testgen)
    fp.close()

# ========================================= Round_Mode = Near Even ===================================================

#if(round_mode=='near_even') :
#   rmode='000'

#if(round_mode=='near_maxMag') :
#   rmode='100'

#if(round_mode=='minMag') :
#   rmode='001'

#if(round_mode=='min') :
#   rmode='010'

#if(round_mode=='max') :
#   rmode='011' 
   
run_testfloat("f32_sqrt","near_even",1,"input.txt")

@cocotb.test()
def run_test_near_even(dut):
    count = 1
    rmode = BinaryValue(int("000",2),n_bits=3,bigEndian=False) 
    fp = open("input.txt","r")
    fp1 = open("near_even_result.txt","w")
    # head = [next(fp) for x in range(N)]
    head =[]
    while True:
        x = fp.readline()
        if not x:
            break
        else:
            head.append(x)
    fp1.write("Operand_1    Expected_result    Expected_flag    Test_result    DUT_result    DUT_flag\n-----------------------------------------------------------------------------------------------------\n")
    for line in range(len(head)):
        field  = head[line].split()
        operand_1 = field[0]
        expected_out = field[1]
        exception_flag = field[2]

        cocotb.fork(Clock(dut.CLK, 10,).start())
        clkedge = RisingEdge(dut.CLK)
        dut.RST_N <= 0
        dut.EN_send <= 0

        for i in range (36):
            yield clkedge
        
        dut.RST_N <= 1
        yield clkedge

        dut.RST_N <= 0
        yield clkedge

        dut.RST_N <= 1
        yield clkedge


        dut.EN_send <= 1


        binOp1 = BinaryValue(int(operand_1,16),n_bits=32,bigEndian=False)
        ExOp   = BinaryValue(int(expected_out,16),n_bits=32,bigEndian=False)
        ExFlag = BinaryValue(int(exception_flag,16),n_bits=5,bigEndian=False)
        binInput = BinaryValue((((binOp1)<<3)+rmode),n_bits=67,bigEndian=False)
        binExOp = BinaryValue(((1<<37)+(ExOp << 5) + ExFlag),n_bits=38,bigEndian=False)
                      
        dut.send_operands = binInput
        yield clkedge

        dut.EN_send <= 0
         
        yield clkedge
        clkcycle=2
        while True:
            if(dut.receive.value[0]==1):
                receive =dut.receive.value
                fh.write("\nRTL OUT: clkcycle: {0} RDY {1} Out: {2}  exp: {3}".format(str(clkcycle),str(dut.RDY_receive.value), hex(receive),hex(binExOp)))
                break
            yield clkedge
            clkcycle+=1
            #receive = dut.receive.value
        
        dut.RST_N = 1
        yield clkedge

        temp0  = receive.binstr
        dutExFlag = BinaryValue(int(temp0[-5:],2),n_bits=5,bigEndian=False)

        temp = BinaryValue(receive>>5,n_bits=33,bigEndian=False)
        dutExOut = temp.binstr
        dutExOut = BinaryValue(int(temp0[1:-5],2),n_bits=32,bigEndian=False)

        dutExOutHex = ((hex(dutExOut))[2:]).upper()
        dutExFlagHex = ((hex(dutExFlag))[2:]).upper()

        dutExOutHex = "0"* (8-len(dutExOutHex))+ dutExOutHex    # 0 padding to match fixed length of 8
        dutExFlagHex = "0"* (2-len(dutExFlagHex))+ dutExFlagHex # 0 padding to match fixed length of 8

        if(receive != binExOp):
            result = "  FAIL \n"
            fh.write(result)
            exit(1)
        else:
            result = "  PASS \n"
            fh.write(result)
        #print("Testcase No. : ",count," Tested..: ","received output.. : ",dutExOutHex,"Expected output.. : ",expected_out,"CLK.. : ",dut.CLK,"RST_N.. : ",dut.RST_N)
        count +=1
        fp1.write(operand_1 +"     " +"     "+ expected_out + "           " + exception_flag + "               "+ 
        result+"           "+dutExOutHex + "      "+dutExFlagHex +"\n") 



