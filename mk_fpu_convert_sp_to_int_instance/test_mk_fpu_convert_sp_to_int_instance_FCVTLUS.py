import random
import sys
import logging as log
import cocotb
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb.monitors import BusMonitor
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure
from testfloat_model import *
import logging as log


fh = open("log_FCVTLUS.txt","w")

class InputDriver(BusDriver):
    """ Drives inputs of DUT. """
    _signals = ["start_operands","start_convert_unsigned","start_convert_long", "EN_start"]
    
    def __init__(self, dut):
        BusDriver.__init__(self, dut, None, dut.CLK)

class InputTransaction(object):
    """Transactions to be sent by InputDriver"""
    def __init__(self, tb, start_operands=0,start_convert_unsigned=0,start_convert_long=0, EN_start=0):
        self.start_operands = BinaryValue(start_operands, tb.start_operands_bits, False)
        self.start_convert_unsigned = BinaryValue(start_convert_unsigned, tb.start_convert_unsigned_bits, False)
        self.start_convert_long = BinaryValue(start_convert_long, tb.start_convert_long_bits, False)
        self.EN_start = BinaryValue(EN_start, tb.EN_start_bit, False)


class InputMonitor(BusMonitor):
    """Observes inputs of DUT."""
    _signals = ["start_operands", "start_convert_unsigned","start_convert_long","EN_start"]

    def __init__(self, dut, callback=None, event=None):
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N,
                            callback=callback, event=event)
        self.name = "in"

    @coroutine
    def _monitor_recv(self):
            clkedge = RisingEdge(self.clock)

            while True:
                yield clkedge
                vec = (self.bus.start_operands.value.integer,self.bus.start_convert_unsigned.value.integer,self.bus.start_convert_long.value.integer,self.bus.EN_start.value.integer)
                fh.write("  start_operands: {0}".format(hex(self.bus.start_operands.value.integer)))
                self._recv(vec)

class OutputTransaction(object):
    """Transaction to be expected / received by OutputMonitor."""
    def __init__(self, tb=None, receive=0, RDY_receive=0,RDY_start=0):
        """For expected transactions, value 'None' means don't care. tb must be an instance of the Testbench class."""
        if receive is not None and isinstance(receive, int):
            receive = BinaryValue(receive, tb.receive_bits, False)
        if RDY_receive  is not None and isinstance(RDY_receive, int):     
            RDY_receive = BinaryValue(RDY_receive, tb.RDY_receive_bits, False)
        if RDY_start  is not None and isinstance(RDY_start, int):     
            RDY_start = BinaryValue(RDY_start, tb.RDY_start_bits, False)
        self.value = [receive]

class OutputMonitor(BusMonitor):
    """Observes outputs of DUT."""
    _signals = [ "receive", "RDY_receive","RDY_start","EN_start"]
    
    def __init__(self, dut, tb, callback=None, event=None):
        """tb must be an instance of the Testbench class."""
        BusMonitor.__init__(self, dut, None, dut.CLK, dut.RST_N, callback=callback, event=event)
        self.name = "out"
        self.tb = tb
   
    @coroutine
    def _monitor_recv(self):
        clkedge = RisingEdge(self.clock)
   
        while True:
            yield clkedge
            if (self.bus.RDY_receive.value==1) and (self.bus.RDY_start.value ==1) and (self.bus.EN_start.value ==1):
                vec =(self.bus.receive.value)
                self._recv(OutputTransaction(self.tb,vec))

                                                     

class Testbench(object):
    fh.write_task='Testbench'
    class MyScoreboard(Scoreboard):
        def compare(self, got, exp, log, **_):
            got1=got.value[0]
            exp1=exp.value[0]
            flag_got=got1 & 0x1F
            flag_exp=exp1 & 0x1F
            got1=got1>>5
            exp1=exp1>>5
            valid=got1>>64
            #log.info("\nExpected: {0!s}.\nReceived: {1!s}.".format(bin(exp1), bin(got1)))
            if valid==1:
                if got1 != exp1:
                    fh.write(" OUTPUT Expected: {0!s}. differ Received: {1!s}. \n".format(hex(exp1), hex(got1)))
                    exit(1)
                elif flag_got != flag_exp:
                    fh.write(" FLAGS  Expected: {0!s}. differ Received: {1!s}. \n".format(bin(flag_exp), bin(flag_got)))
                    exit(1)
                   
                else:
                    fh.write(' passed\n')

    
    def __init__(self, dut):
        self.dut = dut
        self.stopped = False
        self.start_operands_bits = 40
        self.EN_start_bit =1
        self.RDY_start_bits =1
        self.receive_bits =70 
        self.RDY_receive_bits  =1
        self.start_convert_unsigned_bits  =1
        self.start_convert_long_bits  =1
        self.start_rounding_mode_bits  =3
        init_val = OutputTransaction(self)
        self.input_drv = InputDriver(dut)
        self.output_mon = OutputMonitor(dut, self)
        
        # scoreboard on the outputs
        self.expected_output = [init_val]
        self.scoreboard = Testbench.MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)
        
        # Reconstruct the input transactions from the pins
        # and send them to our 'model'
        self.input_mon = InputMonitor(dut)
    

    def stop(self):
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True

def random_input_gen(tb,n=1):
    """
    Generate random input data to be applied by InputDriver.
    Returns up to n instances of InputTransaction.
    tb must an instance of the Testbench class.
    """
    fh.write('INPUT_GEN')
    #inp, exp = run_testfloat('f64_lt', 'near_even', 0, 1, '/home/divya/fbox_tb/fbox-verif/tf_f64_add_1_20200318103931817244.test')
    func_list = ["f32_to_ui64"]
    roundmode_list = ["near_even","near_maxMag","minMag","min","max"]
    #roundmode_list = ["min"]
    for rmode in roundmode_list:
        inp, exp = run_testfloat('f32_to_ui64', rmode, 1, 1)
        if len(inp) == 0 or len(exp) == 0:
            log.error("testfloat did not generate test. Try increasing timeout_value\n")
        fh.write(" Test vectors and expected outputs from model\n")
        fh.write("============================================================================\n")
         
        fh.write("sp_to_int(sp)  :total expected testcases {0!s} total inp {1!s}  , round_mode {2} \n".format(len(exp),len(inp),rmode))

        
        if rmode=='near_even': 
            tb.expected_output.append( OutputTransaction(tb, 0) )
            tb.expected_output.append( OutputTransaction(tb, 0) )
        
        for e in range(len(exp)):
            #fh.write(" -----------",bin(exp[e]))
            tb.expected_output.append( OutputTransaction(tb, exp[e]) )
        for i in range(len(inp)):
            #fh.write_task = 'random_input_gen'
            #fh.write("{0:20s} | === inp transaction === {1} round_mode {2} ".format(fh.write_task, hex(int(inp[i])),rmode))
            yield InputTransaction(tb, inp[i], 1,1,1)
 

@cocotb.coroutine
def clock_gen(signal):
    while True:
        signal <= 0
        yield Timer(5) # ps
        signal <= 1
        yield Timer(5) # ps
    
@cocotb.test()
def run_test(dut):
    cocotb.fork(clock_gen(dut.CLK))
    tb = Testbench(dut)
    dut.RST_N <= 0
    yield Timer(10, units='ps')
    dut.RST_N <= 1
    input_gen = random_input_gen(tb)
    
    # Issue first transaction immediately.
    yield tb.input_drv.send(input_gen, False)
    
    # Issue next transactions.
    for t in input_gen:
        yield tb.input_drv.send(t)
    
    yield tb.input_drv.send(InputTransaction(tb))
    tb.stop()
    yield RisingEdge(dut.CLK)
    
